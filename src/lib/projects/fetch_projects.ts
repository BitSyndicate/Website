export const fetchProjects = async () => {
	const allPostFiles = import.meta.glob('/src/content/projects/*/*.md');
	const iterablePostFiles = Object.entries(allPostFiles);

	const allProjects = await Promise.all(
		iterablePostFiles.map(async ([path, resolver]) => {
			const { metadata }: any = await resolver();
			const postPath = path.slice(11, -3);

			return {
				meta: metadata,
				path: postPath
			};
		})
	);

	return allProjects;
};

