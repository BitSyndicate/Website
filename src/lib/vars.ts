export const siteURL = "https://bitsyndicate.de";
export const siteTitle = "BitSyndicates' Website";
export const siteDescription = "Insights into BitSyndicates' life.";

export const title_text = "BitSyndicate"

export const on_repeat = {
	title: "RUSH! (ARE YOU COMING?)",
	author: "Måneskin",
	link: "https://open.spotify.com/album/2kcJ3TxBhSwmki0QWFXUz8?si=3R7blkdMSEmZ-I3cGwRJAA"
}

export const languages = ["en", "de"]
