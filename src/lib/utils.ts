export const chunk = (arr: any[], size: number) =>
  Array.from({ length: Math.ceil(arr.length / size) }, (_, i) =>
    arr.slice(i * size, i * size + size),
  );

export function splitArray<T>(
  arr: T[],
  condition: (item: T) => boolean,
): { matching: T[]; nonMatching: T[] } {
  const matching = arr.filter((item) => condition(item));
  const nonMatching = arr.filter((item) => !condition(item));
  return { matching, nonMatching };
}
