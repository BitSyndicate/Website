import { splitArray } from "$lib/utils";

export const load = async ({ fetch, params }: any) => {
	let response = await fetch(`/api/posts`);
	let posts = await response.json();
	response = await fetch("/api/projects");
	let projects = await response.json();

	for (let i = 0; i < projects.length; i++) {
		projects[i].meta.pinned_titles = get_pinned(posts, projects[i].meta.pinned)
	}
	if (params.lang) {
		posts = posts.filter((value: any) => { return value.meta.lang == params.lang })
	}
	if (params.lang) {
		projects = projects.filter((value: any) => { return value.meta.lang == params.lang })
	}

	const result = splitArray(projects, (project: any) => {
		return project.meta.status == "finished";
	});
	const finished = result.matching;
	projects = result.nonMatching;

	const translation = await import (`../../translations/${ params.lang ?? 'en' }/translation.json`);
	return {
		posts: posts.slice(0, 5),
		projects,
		finished,
		translation
	};
};

const get_pinned = (posts: any[], pinned: string[]) => {
	const relevants = posts.filter((post: any) =>
		pinned.includes(post.meta.permalink),
	);
	return relevants;
};
