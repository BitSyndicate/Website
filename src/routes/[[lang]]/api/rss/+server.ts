import { siteURL, siteTitle, siteDescription } from "$lib/vars";
import { fetchMarkdownPosts } from "$lib/posts/fetch_posts";

export const prerender = true;

export const GET = async () => {
  const allPosts = await fetchMarkdownPosts();

  const data = allPosts.sort((a: any, b: any) => {
    const left: number = Date.parse(b.meta.updated).valueOf();
    const right: number = Date.parse(a.meta.updated).valueOf();
    return left - right;
  });

  const body = render(data);
  const headers = {
    headers: {
      "Cache-Control": `max-age=0, s-max-age=${600}`,
      "Content-Type": "application/xml",
    },
  };

  return new Response(body, headers);
};

const render = (posts: any[]) => `<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
	<channel>
		<atom:link href="${siteURL}/api/posts/rss" rel="self" type="application/rss+xml" />
		<title>${siteTitle}</title>
		<link>${siteURL}</link>
		<description>${siteDescription}</description>
${posts
  .map(
    (post: any) => `<item>
<guid>${siteURL}${post.meta.permalink}</guid>
<title>${post.meta.title}</title>
<link>${siteURL}${post.meta.permalink}</link>
<description>${post.meta.abstract}</description>
<pubDate>${new Date(post.meta.published).toUTCString()}</pubDate>
</item>`,
  )
  .join("")}
	</channel>
</rss>
`;
