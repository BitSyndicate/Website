import { fetchProjects } from "$lib/projects/fetch_projects";
import { json } from "@sveltejs/kit";

export const GET = async () => {
	const allPosts = await fetchProjects();

	const sortedPosts = allPosts.sort((a: any, b: any) => {
		const left: number = Date.parse(b.meta.updated).valueOf();
		const right: number = Date.parse(a.meta.updated).valueOf();
		return left - right;
	});

	return json(sortedPosts)
};
