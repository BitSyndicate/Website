export const load = async ({ fetch, params }: any) => {
	const response = await fetch(`/api/projects`);
	let projects = await response.json();
	const posts = await fetch(`/api/posts`);
	const allPosts = await posts.json();
	const translation = await import (`../../../translations/${ params.lang ?? 'en' }/translation.json`);

	for (let i = 0; i < projects.length; i++) {
		projects[i].meta.pinned_titles = get_pinned(allPosts, projects[i].meta.pinned)
	}

	if (params.lang) {
		projects = projects.filter((value: any) => { return value.meta.lang == params.lang })
	}
	return {
		projects,
		translation,
		lang: params.lang
	};
};
const get_pinned = (posts: any[], pinned: string[]) => {
	const relevants = posts.filter((post: any) =>
		pinned.includes(post.meta.permalink),
	);
	return relevants;
};
