export const load = async ({ fetch, params }: { fetch: any, params: any }) => {
	const { project } = params;
	const proj = await import(`../../../../content/projects/${params.lang}/${project}.md`);
	const content = proj.default;
	const metadata = proj.metadata;
	const response = await fetch(`/api/posts`);
	const allPosts = await response.json();

	let posts = await get_posts(allPosts, metadata.category);
	if (params.lang) {
		posts = posts.filter((value: any) => { return value.meta.lang == params.lang })
	}

	const pinned = await get_pinned(allPosts, metadata.pinned);
	const translation = await import (`../../../../translations/${ params.lang ?? 'en' }/translation.json`);

	return {
		content,
		posts,
		gallery: metadata.gallery,
		metadata,
		pinned,
		translation
	};
};
const get_posts = async (posts: any[], category: string) => {
	const relevants = posts
		.filter((post: any) => post.meta.categories.includes(category))
		.slice(0, 5);

	return relevants;
};

const get_pinned = async (posts: any[], pinned: string[]) => {
	const relevants = posts.filter((post: any) =>
		pinned.includes(post.meta.permalink),
	);
	return relevants;
};
