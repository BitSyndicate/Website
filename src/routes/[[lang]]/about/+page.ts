export const load = async ({ params }: any) => {
  const page = await import(`../../../content/about/${params.lang ?? "en"}/page.md`);
  return { content: page.default };
};
