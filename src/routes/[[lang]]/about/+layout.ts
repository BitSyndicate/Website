export const load = async ({ params }: any) => {
  const translation = await import(
    `../../../translations/${params.lang ?? "en"}/translation.json`
  );
  return {
    translation,
  };
};
