export const load = async ({ fetch, params }: any) => {
	const response = await fetch(`/api/posts`);
	let posts: any[] = await response.json();
	if (params.lang) {
		posts = posts.filter((value: any) => { return value.meta.lang == params.lang })
	}
	const categories = await get_categories(posts);
	const translation = (await import(`../../../translations/${params.lang ?? 'en'}/translation.json`));
	return {
		posts,
		translation,
		categories: categories.uniqueCategories,
		lang: params.lang,
	};
};

const get_categories = async (posts: any[]) => {
	const uniqueCategories: { [key: string]: any } = {};
	posts.forEach((post: any) => {
		post.meta.categories.forEach((category: string) => {
			if (Object.prototype.hasOwnProperty.call(uniqueCategories, category)) {
				uniqueCategories[category].count += 1;
			} else {
				uniqueCategories[category] = {
					title: category,
					count: 1,
				};
			}
		});
	});

	const sortedUniqueCategories = Object.values(uniqueCategories).sort((a, b) => (a.title > b.title ? 1 : -1));

	return {
		uniqueCategories: sortedUniqueCategories,
	};
}
