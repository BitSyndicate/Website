// src/routes/blog/category/[category]/+page.js
export const load = async ({ fetch, params }: any) => {
  const { category } = params;
  const response = await fetch(`/api/posts`);
  const allPosts = await response.json();

  const posts = allPosts.filter((post: any) => {
    return post.meta.categories.includes(category) && (params.lang
      ? post.meta.lang == params.lang
      : true);
  });

  const translation = await import(
    `../../../../../translations/${params.lang ?? "en"}/translation.json`
  );

  return {
    translation,
    category,
    posts,
  };
};
