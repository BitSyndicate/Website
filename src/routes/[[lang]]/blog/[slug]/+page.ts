export async function load({params}: { params: any }) {
	const post = await import(`../../../../content/posts/${params.lang}/${params.slug}.md`);
	const { title, abstract, author, permalink } = post.metadata;
	const content = post.default;
	const categories = post.metadata.categories;
	const image =  { image: post.metadata.image, image_alt: post.metadata.image_alt };
	const date = { published: post.metadata.published, updated: post.metadata.updated }
	const translation = await import (`../../../../translations/${ params.lang ?? 'en' }/translation.json`);

	return {
		content,
		title,
		permalink,
		author,
		date,
		image,
		abstract,
		categories,
		translation,
		lang: params.lang
	};
}
