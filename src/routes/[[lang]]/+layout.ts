export const prerender = true;

export const load = async ({ url, params }: any) => {
  const currentRoute = url.pathname;
  const translation = await import(
    `../../translations/${params.lang ?? "en"}/translation.json`
  );
  return {
    currentRoute,
    translation,
    language: params.lang,
  };
};
