---
title: "Die App-Inventor-Quiz-App"
category: "appinventor quiz app"
pinned: []
description: "Eine Quiz-App implementiert mit dem MIT-Appinventor als Prüfungsersatz in der 10. Klasse"
status: "running"
image: "/content/projects/appinventor/appinventor_logo.jpg"
image_alt: "Logo des MIT-Appinventors"
permalink: "/de/projects/appinventor"

lang: "de"
keywords:
  - appinventor
  - quiz app
  - school

gallery:
  [
    { "image": "/content/projects/appinventor/appinventor_logo.jpg", "alt": "appinventor_logo" },
    { "image": "/content/projects/appinventor/login-awaiter.png", "alt": "Code-Block" },
    { "image": "/content/projects/appinventor/login-button-code.png", "alt": "Code-Block" },
    { "image": "/content/projects/appinventor/login.png", "alt": "Login View" },
    { "image": "/content/projects/appinventor/make-username-password-request.png", "alt": "Code-Block" },
    { "image": "/content/projects/appinventor/schema.png", "alt": "Database schema" },
    { "image": "/content/projects/appinventor/server.jpg", "alt": "Server image" },
    { "image": "/content/projects/appinventor/startpage-score-view.png", "alt": "Code-Block" },
    { "image": "/content/projects/appinventor/startpage.png", "alt": "The startpage" },
    { "image": "/content/projects/appinventor/wrong.png", "alt": "Incorrectly answered question" },
    { "image": "/content/projects/appinventor/correct.png", "alt": "Correctly answered question" },
    { "image": "/content/projects/appinventor/question.png", "alt": "Image of a question" }
  ]
---

## Worum geht es?

Bei diesem Projekt geht es darum, eine Quiz-App mit dem MIT-Appinventor zu 
implementieren. Diese Quiz-App soll die Darstellung, Beantwortung und bepunktung
von Fragen erlauben. Um dieses Ziel zu erreichen habe ich eine Server-Client
Architektur entwickelt, bei welcher der Server die Fragen und die Punktestände
der Nutzer verwaltet und der Client den Nutzer abfragt und die Daten schön darstellen
kann.
