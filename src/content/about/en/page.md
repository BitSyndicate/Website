## Hey there!

I am a 15 year old hacker from Germany. I enjoy low-level programming, math and
physics. I am currently studying Computer Science at the University of Bonn. If 
you want to accompany me on my journey, stay tuned.

Topics I am currently interested in include gamedev, Unix and federated platforms.
