---
title: "Der Quizflow"
author: "BitSyndicate"
published: "2024-04-15T14:49:00"
updated: "2024-04-15T14:49:00"
abstract: "Jetzt geht es wirklich los!"
permalink: "/de/blog/appinventor-quiz-app-quizflow"
image: "/content/projects/appinventor/eval.png"
image_alt: "Evaluation Screen"

lang: "de"

categories:
  - appinventor quiz app
  - school
  - MIT appinventor
  - quiz app
---

## Was ist zu tun?

1. Den Nutzer die abzufragende Kategorie auswählen lassen
1. 20 zufällige Fragen aus dieser Kategorie in Zufälliger reihenfolge abfagen
1. Den erreichten Punktestand ausrechnen und hochladen
1. Dem Nutzer eine Rückmeldung geben

## Zu abfagende Kategorie auswählen

Hierzu habe ich den Screen `start_quiz` erstellt, der nur dem Zweck dient, 
den Spiel-flow zu vorinitialisieren. Hier sucht sich der Nutzer die Kategorie
aus.

![Kategorie Aussuchbildschirm](/content/projects/appinventor/categories.png)

Dies wird wie schon beim [Leaderboard](/de/blog/appinventor-quiz-app-leaderboard) 
durch eine `ListView` und einer API-Anfrage gelöst. Wenn der Nutzer sich 
nun die Kategorie ausgesucht hat wird der Screen `quiz` mit dem Kategorienamen
gestartet.


## Die Fragen aussuchen und abfragen.

Sobald der `quiz`-Screen initialisiert wird, fragt er die 
`/categories/{name}/questions`-Resource an. Danach sucht die App sich 20 
zufällige Elemente aus dieser Liste und packt sie in die abzufragende Liste, 
der Pseudocode hierfür sieht so aus:

```pseudocode
Q = Die Fragen
L = Länge Q
A = AusgabeListe
repeat min{L, 20} times 
  füge zu A ein zufälliges Element aus Q hinzu und entferne dieses aus Q
```

Nun startet das Programm eine Uhr, welche jede Sekunde ein Signal sendet, außer 
falls das Programm minimiert wird. Diese Uhr wird verwendet um die Zeit, welche
der Spieler für jede Frage braucht zu ermitteln, da der Nutzer maximal 20s pro 
Frage verbringen darf und die verbrauchte Zeit in die Bewertung mit einfließt.

Falls der Nutzer eine Frage beantwortet wird ermittelt ob der Nutzer richtig lag,
indem der `correct`-Index der Frage mit dem Index der Antwort, welche der Nutzer
gegeben hat, abgeglichen wird, da der MIT-Appinventor 1 basierte Indizierung 
verwendet muss eins hinzuaddiert werden. Falls der Nutzer richtig lag wird die
bisherig gesammelte Punktzahl, welche bei Null startet um die folgende Formel 
erhöht, wobei s die vergange Zeit für diese Frage beschreibt:

*⌊(20 - s) / 5⌋ + 1*

Nutzer, welche schneller Antworten werden also belohnt.

Außerdem wird als User-Feedback der Hintergrund in entweder Rot oder Grün gefärbt,
und falls der Nutzer falsch liegt, wird ihm die richtige Antwort angezeigt. Das 
Spiel wartet vor dem Anzeigen den nächsten Frage auch noch eine Sekunde.

![Bild einer Frage](/content/projects/appinventor/question.png)

![Bild einer richtigen Antwort](/content/projects/appinventor/correct.png)

![Bild einer falshcen Antwort](/content/projects/appinventor/wrong.png)

## Den erreichten Punktestand hochladen und Anzeigen

Sobald der Nutzer alle bis zu 20 Fragen zu beantworten, wird er zum Screen 
`eval` Umgeleitet, wobei der erreichte Punktestand zusätzlich zum Kategorienamen 
ein Startparameter ist.

Der Screen ist dafür zuständig den Score des Nutzers an den Server zu senden 
und danach vom Server die größte bisherige Punktzahl anzufragen.

In  pseudocode sieht das dann so aus:

```pseudocode 
set label: score.text to startvalue_score

prepare_http_request = {
  method: POST,
  route: "/user/{startvalue_category}/score"
  headers: [
    { "Content-Type", "application/json" },
    { "Authorization", "<users session token>" }
  ],
  body: {
    json: {
      score: <reached score>
    }
  }
}

send_request(prepare_http_request)

best_score = get_score_from_api()
set label: personalbest.text to best_score
```

![Eval Schirm](/content/projects/appinventor/eval.png)
