---
title: "Die Serverspezifikation" 
author: "BitSyndicate"
published: "2024-04-13T14:25:00" 
updated: "2024-04-13T14:25:00"
abstract: "Die Spezifikation wie sich der Server nach außen hin verhält."
permalink: "/de/blog/appinventor-quiz-app-server-spec"
image: "/content/projects/appinventor/server.jpg"
image_alt: "Bild eines Server-Schrankes"

lang: "de"

categories:
- appinventor quiz app
- school 
- quiz app
- API design
---

Der Server ist dafür verantwortlich die Nutzerkonten und die Inhalte zu 
verwalten. Damit dieser mit der Appinventor-App kommunizieren kann habe ich mich
für eine API auf JSON-Basis entschieden. Um die API jetzt auszuprobieren kann man
mit Tools wie `curl` schon mal die API-Endpunkte mit dem Prefix 
`https://bitsyndicate.de/appinventor` benutzen.

## Funktionalität

Der Server sollte die folgenden Anforderungen erfüllen:

- Nutzerkonten anlegen und löschen können 
- Nutzer anmelden und authentifizieren können
- Nutzern erlauben Punkte zu sammeln
    - Nutzern erlauben ihren Punktestand zu speichern
    - Nutzern erlauben ihren Punktestand abzurufen
- Das Abrufen von Kategorien und Fragen ermöglichen
- Punkte für Kategorien zu speichern damit man 
- Das Abrufen von Bestenlisten im Bezug auf alle Kategorien summiert und einer
einzelnen Kategorie.

### API-Layout

#### Warnung

Diese API ist nicht sehr Hacking-resistent (im Sinne von, sehr leicht Auszunutzen).
Damit sie das wäre müsste ich alles Serverseitig auswerten. Wenn ich das jedoch 
tuen würde, würde der Appinventor-Anteil dieses Projektes nur noch einer WebView
entsprechen. Deshalb habe ich mich entschieden die API so beizubehalten.

#### Vorwissen

##### HTTP

Die API ist auf dem HTTP *(Hyper Text Transfer Protocol)* aufgebaut. Dies ist 
essenziel ein Protokol wie man über das Netzwerk Dateien vom Client zum Server 
und zurück versendet. Hierbei iniziert der Client immer die Verbindung und sollte
laut Spezifikation eine Antwort vom Server bekommen. Der Umfang in welchem dieses
Protokol in diesem Projekt verwendet wird ist jedoch nicht sehr breit ausgefächert.

Wenn ich über eine Anfrage (Request) spreche meine ich, dass der Client dem 
Server eine Anfrage mit oder ohne Körper, schickt auf welche der Server Antwortet.
Hierbei ist die Anfrage und die Antwort in jeweils zwei Teile unterteilt:

1. Der Header (Kopf), welche eine Beschreibung von Metadaten als Schlüssel-Werte
Paare darstellt
2. Der Body (Körper), welcher die zu übertragede Datei darstellt. Unter umständen
ist dieser leer.

Wenn ich GET- oder DELETE-Request spezifiziere meine ich eine Anfrage, welche 
keinen Körper besitzt und den Server nach einer Resource fragt, bzw. Pfad: `/url`.

Wenn ich über POST-Requests spreche meine ich, dass der die Anfrage einen Körper
mit Daten besitzt, welche an eine Resource, bzw. Pfad `/usr` geschickt werden um
Zustand im Server zu ändern.


##### Json

Die API ist nach HTTP-Primitiven und JSON-Schemata strukturiert. Ein JSON-Schema
ist ein Weg anzugeben wie Daten strukturiert sein sollen.

```json 
{
    "abc": [string],
    "cdf": {
        "a": bool,
        "d": number,
        "c": string,
        "list": [
            { object }
        ]
    }
}
```

Json Nutzt primitive Datentypen und Objekte um Komplexe Strukturen auszudrücken
Das Wurzel-Element ist immer ein Objekt somit würde man das obrige Schema folgend
lesen: 

> Das JSON-Wurzel-Objekt enthält den Schlüssel `abc`, welchem eine Liste an Strings
> (Text) zugewiesen wird. Darauf folgt der Schlüssel `cdf`, welcher ein weiteres
> Objekt enthält. Das Objekt welches von `cdf` enthalten wird, besitzt die Schlüssel
> `a`, `d`, `c` und `list` welche jeweils ein Wahrheitswert (wahr/falsch), eine Zahl, 
> ein beliebige string und eine Liste an weiteren Objekten darstellen.

##### Authentifikation

Wenn ich sage, dass eine Resource Authentifikation benötigt, dann muss die Anfrage
in ihrem `Authorization`-Reader ein valides Session-Token beinhalten, welches 
der Server bei Login und Registrierungs-Anfragen sendet.

```HTTP
Authorization: <token>

```

#### Die API-Endpunkte

##### Nutzer erstellen

Um einen Nutzerkonto anzulegen muss man eine POST-Request an die `/user/register`
Resource mit folgendem JSON-Schema ausgefüllt, schicken:

```json
{
	"username": string, 
	"password": string
}
```

Wenn alles funktioniert antworet der Server wie folgt.
```json
{
    "session_id": string
}
```
##### Nutzer anmelden

Um sich anzumelden sendet der Nutzer das folgende Schema mit POST-Request an 
die `/user/login` Resource.
```json
{
	"username": string,
	"password": string
}
```

Wenn alles funktioniert antworet der Server wie folgt.
```json
{
    "session_id": string
}
```

##### Abmeldung (Authentifikation notwendig)

Um sich abzumelden sendet der Nutzer eine DELETE-Request an die `/user/logout`
Resource.

##### Konto löschen (Authentifikation notwendig)

Um den Nutzeraccount zu löschen sendet der Nutzer eine DELETE-Request an die
`/user` Resource.

##### Punktestand des Nutzers (Authentifikation notwendig)

Um seinen Punktestand für alle Kategorien zu kriegen, sendet der Nutzer eine 
GET-Request an die `/user/score` Resource. Der Server antwortet wie folgt:

```json
{
    "score": number
}
```

###### Den Punktestand des Nutzers für eine Kategorie (Authentifikation notwendig)

Um seinen Punktestand für eine bestimmte Kategorie abzurufen sendet der Nutzer
eine GET-Request an die `/user/{category_name}/score` Resource, wobei 
category_name durch den Namen der Kategorie, welche in Frage steht ersetzt wird.

Der Server antworet mit dem selben Schema wie oben.

##### Alle Kategorien abrufen

Um alle Kategorien abzurufen muss man eine GET-Request an die `/category`-Resource
senden. Der Server antwortet wie folgt:

```json
{
    "categories": [
        { "name": string, "display": string },
    ]
}
```
[1] `display` beschreibt den Anzeignamen der Kategorie. `name` beschreibt den 
wirklichen Namen der Kategorie wenn der Client mit dem Server kommuniziert.

##### Alle Fragen eine Kategorie abrufen

Um alle Fragen einer Kategorie abzurufen mus man eine GET-Request an die 
folgende Resource schicken: `/category/{category_name}/questions`, wobei 
category_name durch den Namen der Kategorie ersetzt werden muss.

Der Server antworet wie folgt:

```json
{
    "questions": [
        { "question": string, "answers": [string], "correct": number },
    ]
}
```
[1] `question` beschreibt den Fragentext. `answers` beschreibt die 
Beantwortungsoptionen. `correct` beschreibt das wievielte Element in der 
Antwortenliste korrekt ist, wobei das erste Element als das Nullte beschrieben
wird.

##### Die Bestenliste abrufen

Um die Bestenlisten mit allen Kategoriepunkten aufsummiert abzurufen muss man 
eine GET-Request an die `/leaderboard`-Resource senden.

Der Server antworet wie folgt:

```json
{
    "board": [
        { "username": string, "score": number },
    ]
}
```

###### Get Category leaderboard

Um die Bestenlisten einer spezifischen Kategorie abzurufen muss man eine 
GET-Request an die `/category/{category_name}/leaderboard`-Resource senden. 
Man erhält das selbe Schema wie oben beschrieben.

##### Store points (authentication required)

Um den Punktestand eines Nutzers nach einer Quiz-Runde zu speichern muss
der Client eine POST-Request mit dem folgenden Inhalt and den Server senden.
Falls der von Client gesendete Punktestand höher ist als der bisher gespeicherte
wird der Punktestand aktualisisert.

```json 
{
    "score": number
}
```
