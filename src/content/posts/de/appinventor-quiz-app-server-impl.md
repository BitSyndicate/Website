---
title: "Ein Einblik in die Server Implementation"
author: "BitSyndicate"
published: "2024-04-13T15:25:00"
updated: "2024-04-13T15:25:00"
abstract: "Wie der Server meiner MIT-Appinventor-Quiz-App unter der Haube funktioniert."
permalink: "/de/blog/appinventor-quiz-app-server-impl"
image: "/content/projects/appinventor/server.jpg"
image_alt: "Bild eines Server-Schrankes"

lang: "de"

categories:
  - appinventor quiz app
  - schol
  - quiz app
  - SQL
---

Dieser Post setzt voraus, dass Sie die
[Spezifikation](/de/blog/appinventor-quiz-app-server-spec) schon gelesen haben.

In diesem Artikel schauen wir uns an, wie die Serverimplementation funktioniert.
Zu diesem Zweck schauen wir uns zuerst die verwendete Datenbank an, welche das
Herzstück des Servers bildet.

![Datenbank Schema visualisiert](/content/projects/appinventor/schema.png)

> Die Datenbank ist eine SQL-basierte relative Tabellen-Datenbank.

## Nutzer

Die `user`- und `sessions` Tabelle sind das Rückrad der Nutzerverwaltung. Jetzt
schauen wir uns diese mal genauer an.

### Die `user`-Tabelle

```sql
CREATE TABLE users(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	username TEXT UNIQUE NOT NULL,
	password TEXT NOT NULL
);
```

Jeder Eintrag in der Tabelle, also jeder Nutzer hat eine einzigartige (`UNIQUE`)
id-Nummer (`INTEGER`), welche von der Datenbank automatisch vergeben (`PRIMARY KEY`)
wird und immer existieren muss (`NOT NULL`), mit diesem wird in anderen Tabellen
auf den Nutzer verwiesen. Jeder Nutzer hat einen einzigartigen immer
vorhandenen Nutzernamen und ein Passwort, welches als Hash [1] gespeichert
wird.

Wenn also ein neuer Nutzerkonto erstellt wird. Wird geschaut ob der Nutzername
schon existiert und wenn nicht wird das Passwort gehashed (der Hash des Passworts
wird berechnet) und der Nutzer wird dieser Tabelle hinzugefügt.

[1] Ein Hash bezeichnet eine Form der irreversibelen Veränderung von Daten.

### Die `sessions`-Tabelle

Die sessions-Tabelle ist ein wichtiger Bestandteil der Nutzerverwaltung und
dient der Verfolgung aktiver Sitzungen für jeden Benutzer.

```sql
CREATE TABLE sessions(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	user_id INTEGER NOT NULL,
	session_id TEXT UNIQUE NOT NULL,
	FOREIGN KEY (user_id) references users(id) ON DELETE CASCADE
);
```

Jeder Eintrag in dieser Tabelle erhält eine eindeutige Identifikationsnummer
(`id`), die als Primärschlüssel dient immer vorhanden sein muss. Die `id` wird
automatisch von der Datenbank generiert.

Die Spalte `user_id` speichert die `ID` des Benutzers, dem die Sitzung gehört,
muss immer existieren. Durch die Definition eines Fremdschlüsselbezugs zur
`users`-Tabelle wird sichergestellt, dass die `user_id` auf eine gültige
Benutzer-ID in der users-Tabelle verweist. Diese Referenz wird verwendet, um
sicherzustellen, dass Sitzungen nur für existierende Benutzer erstellt werden
können. Zudem wird durch die Option `ON DELETE CASCADE` festgelegt, dass beim
Löschen eines Benutzers auch alle zugehörigen Sitzungen automatisch gelöscht
werden, damit keine ungültigen Referenzen verbleiben.

Die Spalte `session_id` speichert eine eindeutige Kennung für die Sitzung.
Diese Kennung wird dem Nutzer mitgeteilt damit sie abgeglichen werden kann,
um Sicherzugehen, dass er autorisiert ist, auf geschützte Ressourcen zuzugreifen.
Die `session_id` muss ebenfalls eindeutig sein und muss für jeden Eintrag existieren

Um also nun einen Nutzer eine Sitzung zu geben, generiert die Server software
eine SessionID, eine zufällige folge an Buchstaben, welche er dem Nutzer mitteilt
und selber speichert. Dies hat den vorteil, dass einzelne Sitzungen gelöscht werden
können ohne das Passwort zu ändern.

## Der Inhalt bzw. die Daten.

Die Fragen und Kategorien werden ebenfalls in der Datenbank gespeichert, in dem
der Server folgendes JSON-Schema auswertet und einzigartig in die Datenbank einfügt:

```json
{
    "categories": [
        {
            "display": string,
            "name": string,
            "questions": [
                {
                    "question": string,
                    "answers": [
                        { "text": string, "correct": bool }
                    ]
                }
            ]
        }
    ]
}
```
---
`display` und `name` bezeichnen den Namen und den Anzeigenahmen der Kategorie.

`question` bezeichnet den Fragetext.

`text` bezeichnet den Antwortsmöglichkeitstest und `correct` ob die Antwort 
korrekt ist

---

Obriges Schema wird letzentlich in diese Tabellen eingefügt:

```sql 
CREATE TABLE categories(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	name TEXT UNIQUE NOT NULL,
	display TEXT NOT NULL
);
CREATE TABLE questions(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	question TEXT NOT NULL,
	category_id INTEGER NOT NULL,
	FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE
);
CREATE TABLE answers(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	is_correct INTEGER NOT NULL, -- BOOLEAN 0 = false, x != 0 = true
	text TEXT NOT NULL,
	question_id INTEGER NOT NULL,
	FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE
);
```

Wie man sieht, müssen hier auch alle Spalten bei jeem Eintrag vorhanden sein.

### Die `categories`-Tabelle

Diese Tabelle beschreibt welche Kategorien existieren. Hierbei ist erforderlich,
dass die Kategorie einen einzigartigen Namen und einen Anzeigenahmen besitzt. Die
Id, die ähnlich wie bei der `users(id)` als Referenz verwendet wird, wird von 
der Datenbank automatisch generiert.

Diese Tabelle wird bei Anfragen zum Abrufen aller Kategorien verwendet.

### Die `questions`- und die `answers`-Tabelle

Die Fragen-Tabelle beschreibt alle Fragen, die die Quiz-App kennt. Jede Frage hat 
zum referenzieren in anderen Tabellen eine `id`, einen Fragetext und referenziert
die Kategorie zu welcher sie gehört. Falls diese Kategorie gelöscht wird, wird 
die Frage mit gelöscht.

Die Antworten-Tabelle beschreibt alle Antwortmöglichkeiten aller Fragen. Hierbei
wird eine Referenz-ID, ein Indikator ob die Antwort korrekt ist, der Text der 
Antwort wie die Frage zu welcher die Antwort gehört gespeichert. Wenn die Frage
gelöscht wird, werden alle zugehörigen Antworten gelöscht.

Diese Tabellen wird abgefragt wenn man alle Fragen einer Kategorie sucht in dem 
man alle Fragen ausließt, welche die in Frage stehende Kategorie referenzieren.
Wenn man dann alle FragenIds die man abfragen muss kennt, fragt man die 
`answers`-Tabelle nach diesen FragenIds ab und ermittelt die Korrekten antworten 
für jede Frage.

## Das Punktestandmanagement

```sql
CREATE TABLE scores(
	id INTEGER PRIMARY KEY UNIQUE NOT NULL,
	score INTEGER NOT NULL,
	user_id INTEGER NOT NULL,
	category_id INTEGER NOT NULL,
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE
);
```

Für jeden Nutzer wird Der Punktestand bei einer Kategorie gespeichert. Falls der
Nutzer oder die Kategorie gelöscht werden, wird der Eintrag in der `scores`-Tabelle
mitgelöscht. Um den totalen Punktestand des Nutzers über alle Kategorien hinweg
zu ermitteln summiert man alle `score`-Werte, welche die `user_id` des Nutzers 
in Frage darstellen. Wenn man jetzt z. B. die Bestenliste ermitteln möchte gruppiert
und summiert man alle Einträge eines Nutzers in dieser Tabelle und sortiert nach dem 
gesammten score und danach, falls zwei Nutzer den selben Punktestand haben, nach
dem Nutzernamen.

Exkurs: Die Programmlösung, macht das was oben beschrieben wurde als einzelne 
Datenbank-Anfrage
```sql 
SELECT users.username, SUM(scores.score) AS score 
    FROM users JOIN scores ON scores.user_id = users.id 
    GROUP BY users.username 
    ORDER BY score DESC, users.username ASC;
```


## Fazit

Der Server hier ist die Instanz, welche mithilfe dieser Datenbank den Zustand 
der Quiz-App aufrecht erhält. Alle API-Operationen wurden hier beschrieben, 
deshalb werde ich nicht im einzelnen Erläutern, was der Server genau tut, dies
würde nähmlich den Umfang dieser Dokumentation sprengen und wäre relativ unnötig.
Bei Interesse werde ich demnächst den Source-Code verlinken.
