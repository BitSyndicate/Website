---
title: "Das Leaderboard"
author: "BitSyndicate"
published: "2024-04-13T18:44:00"
updated: "2024-04-15T14:48:00"
abstract: "Das Einrichten einer anständigen Leaderboard-View"
permalink: "/de/blog/appinventor-quiz-app-leaderboard"
image: "/content/projects/appinventor/leaderboard.png"
image_alt: "Bild eines Server-Schrankes"

lang: "de"

categories:
  - appinventor quiz app
  - school
  - MIT appinventor
  - quiz app
---

Das Leaderboard ist einer der wichtigsten Teile der Quiz-App. Es erlaubt Nutzern
sich mit anderen zu vergleichen und schenkt ihnen voraussichtlich Motivation.

## Anforderungen

- Eigene Leaderboards für jede Kategorie
- Ein globales Leaderboard was alle anderen beinhält.

## Leaderboards anfragen und anzeigen

Für das Anzeigen des Leaderboards verwenden wir im Appinventor eine `ListView`
und zum abfragen welche Kategorie es denn genau sein soll verwenden wir einen 
`ListPicker`. Diese Elemente können den Inhalt von Listen mithilfe ihres 
`Element`-Wertes darstellen. Prorammatisch setzt man dies so:

![set LISTVIEW.Element to input_list](/content/projects/appinventor/set_elements.png)

Wie ladet man nun die Daten für das Leaderboard? Der folgende Pseudocode 
beschreibt die Antwort:

```pseudocode
Finde raus welche Leaderboards existieren

Führe aus, falls das Leaderboard geupdated wird:
  Finde raus welches Leaderboard genutzt werden soll, 
    falls keine Angaben existieren, nehme das Standard leaderboard.
  Mache eine API-Request um das Leaderboard zu kriegen.
  Wandle das JSON in eine Liste mit dem Textformat `<punktestand> Punkte: 
    <nutzername>` um.
  Setze die ListView auf diese Liste.
```

Wie findet man nun Raus welche Kategorien existieren:

```pseudocode
Frage die `/category` Resource an und setze die ListPicker-Liste auf die 
Anzeige namen dieser Kategorien und speichere ihre Namen in einem 
Schlüssel-Werte- Speicher, wobei der Schlüssel der Anzeigename und der Wert der 
Name sein soll.
```

Wie ermittelt man nun welche Kategorie angezeigt werden soll und ob das 
Leaderboard geupdated werden muss?

```pseudocode
Beim öffnen des Screens zeige die standard /leaderboard resource an.

Wenn der Nutzer eine Kategorie auswählt, frage die /category/{name}/leaderboard 
resource an um das neue Leaderboard anzuzeigen, falls der Nutzer die Option 
`Keine` nimmt, zeige ihm wieder /leaderboard
```

So sieht das Ganze dann im Appinventor aus:

![Beschriebenes als Blockly](/content/projects/appinventor/init-leaderboard.png)

![Beschriebenes als Blockly](/content/projects/appinventor/leaderboard-choose-category.png)

![Beschriebenes als Blockly](/content/projects/appinventor/leaderboard-handler.png)


## Bilder


![cateogry picker](/content/projects/appinventor/leaderboard-category-picker.png)

![history leaderboard](/content/projects/appinventor/leaderboard-history.png)

![general leaderboard](/content/projects/appinventor/leaderboard.png)
