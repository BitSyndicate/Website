---
title: "Die Startseite"
author: "BitSyndicate"
published: "2024-04-13T17:55:00"
updated: "2024-04-14T17:44:00"
abstract: "Wie soll sich denn die Startseite Verhalten? Finde es raus!"
permalink: "/de/blog/appinventor-quiz-app-startpage"
image: "/content/projects/appinventor/startpage.png"
image_alt: "Bild der Startseite"

lang: "de"

categories:
  - appinventor quiz app
  - school
  - MIT appinventor
  - quiz app
---

## Anforderung

Die Startseite ist der zentrale Anfangspunkt jedes Nutzers. Alle Informationen
und wege zu Informationen sollten gut sichtbar geboten werden. Der Nutzer sollte
sich auch hier direkt Anmelden oder Registrieren können.

## Anmeldung

Damit man den Nutzer anmelden oder registrieren kann. Brauch man laut unserer API,
wie in der [Spezifikation](/de/blog/appinventor-quiz-app-server-spec) dargelegt einen
Nutzernamen und ein Passwort. Ein Nutzer der nicht angemeldet ist bringt einem auch
nichts, deshalb habe ich mich dafür entschieden, falls der Nutzer nicht angemeldet
ist, ihm diesen Bildschirm anzuzeigen:

![Anmeldungsseite mit Nutzernamen- und Passworteingabe wie auch einem Registrierungscheckbox](/content/projects/appinventor/login.png)

Wie man sieht hat der Nutzer die Option sich anzumelden oder sich zu registrieren,
je nachdem ob er die Checkbox Registrieren betätigt. Wenn er sich dann anmeldet 
passiert folgendes:

1. Es wird geschaut, ob der Nutzer sich anmelden möchte oder nicht.
1. Je nachdem ob er sich anmelden möchte wird eine Registrations- oder 
Anmeldungsanfrage an den Server gesendet.
1. Falls alles erfolgreich ist, erhält man eine `session_id`. Ansosten wird dem 
Nutzer eine Fehlermeldung vor die Nase geschoben.

In Blockly (der Appinventor-Programmiersprache) sieht dieses Verhalten dann so aus:

** Der in der Datenbank gespeicherte `API_URL`-Schlüssel entspricht dem aktuellen 
Host jener, also [`https://bitsyndicate.de/appinventor`](https://bitsyndicate.de/appinventor). 
Dies dient der Lesbarkeit und Wartbarkeit des Codes **

![obrig Beschriebenes in Blockly Punkt 1 und 2](/content/projects/appinventor/login-button-code.png)

Dieser Block entscheidet ob sich registriert oder angemeldet wird anhand der 
`register`-Checkbox und setzt die angefragte Resource so richtig.

![der Block Make-Username-Password-Requeset](/content/projects/appinventor/make-username-password-request.png)

Der obrige Block ist dafür zuständig die richtige Anfrage für die API zu formulieren
hierzu setzt er die URL auf die, die ihm der vorherige Block übergibt und füllt ansonsten 
das Dictionary, welches wir zu JSON übersetzen aus und macht die POST-Request auf 
die spezifizierte Resource. Der `Content-Type` welcher den `Request-Headers` zugewiesen
wird drückt einfach nur aus, dass wir JSON senden werden.

![obrig Beschriebenes in Blockly](/content/projects/appinventor/login-awaiter.png)

Dieser Block ist dafür Zuständig alle Antworten von HTTP-Anfragen zu verarbeiten.
Damit wir die richtige Antwort hierdrin verarbeiten können. Falls die url entweder 
der Registrations- oder Anmeldungs-URL entspricht prüfen wir ob die Anfrage erfolgreich 
war, also der `responseCode = 200`. Falls dies der fall ist, wird die vom Server
gesendete `session_id` unter `session` global für das Programm gespeichert.

Hier nach wird die Startseite nochmal ausgefüllt. Wobei jetzt die Anmelde-Seite 
unsichtbar gemacht wurde und der Hauptdialog sichtbar geworden ist. Hier kann 
der Nutzer nun die Tutorial-, StartQuiz- und Leaderboard-Bildschirme öffnen. Die
Abmelden und Konto löschen Knöpfe machen genau das was sie sagen. Senden eine 
DELETE-Request vom aktuellen Nutzer an die API-Endpunkte.

![Startseite](/content/projects/appinventor/startpage.png)

Aber wie wird jetzt der Punktestand des Nutzers angezeigt? Naja beim Neuladen 
des Screens wird, falls der Nutzer authentifiziert ist mithilfe von den 
folgenden Code-Blöcken eine Anfrage an den `score`-API-Endpunkt gesendet und 
der Punktestand wird angezeigt.

![Starpage Score viewer](/content/projects/appinventor/startpage-score-view.png)

Dieser Ausschnitt stammt ebenfalls von dem `Web1.GotText`-Eventhandler. Wenn 
die Anfrage erfolgreich war wird der score zu dem `score`-Label geschrieben. Falls
nicht, wird die Session gelöscht, da das laut Spezifikation die einzige möglichkeit
ist, dass falls man eine `session_id` hat, diese Resource nicht `200 OK` antwortet.

## Die Tutorial-Seite

Die Tutorial-Seite stellt eine kleine Erläuterung dar, wie man die Quiz-App
benutzt und wie die Bewertung funktoiniert.

![Tutorial-Seite](/content/projects/appinventor/tutorial-page.png)

