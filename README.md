# My Website

## Create a post

To create a post go to the `src/content/posts` directory, select the language 
the post is in. Go into the following directory and create a markdown file with 
the name as the slug of the post. Followingly fill out this header:
```md
---
title: "<title of the post>"
author: "<author of the post>"
published: "<publishing date as YYYY-MM-DDTHH:mm:ss.sssZ" # T: literal
                                                          # Z: timezone offset
                                                          # HH: 0-23 clock
                                                          # only YYYY-MM-DD are 
                                                          # necessary, specificness 
                                                          # may be omitted
updated: "<date of last update as YYYY-MM-DDTHH:mm:ss.sssZ"
abstract: "<summary of the article"
permalink: "<permalink of the article ({lang}/blog/slug(without .md)))>"
image: "<a title image uri>"
image_alt: "<alt text of the image>"

lang: "<language (2 letter)>"

categories:
- <categories this post belongs to>
---
```

## Create a Project

To create a project go to the `src/content/posts` directory, select the language 
the project is in. Go into the following directory and create a markdown file with 
the name as the slug of the project. Followingly fill out this header:

```markdown
---
title: "<name of the project>"
category: "<the corresponding blog category>"
pinned: ["<permalinks to pinned blogposts>"]
description: "<short description of the project"
status: "<project status either idea, paused, waiting, running, abandoned, or finished>"
image: "<an image for the project>"
image_alt: "<alt text of the project image"
permalink: "<permalink of the project ({lang}/projects/slug(without .md)))>"

lang: "<language (2 letter)"
keywords:
    - keywords about this project

gallery:
  [
    { image: "<url of first image of gallery", alt: "alt text of first image in gallery" }
  ]
---
```
